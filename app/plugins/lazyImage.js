import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import error from '~/static/images/default/defaultTask.jpg'
Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 1,
  observer: true,
  error,
})
