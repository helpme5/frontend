import https from 'https'
import qs from 'qs'
import create from '../api/repository'

export default ({ $axios, app, store, redirect }, inject) => {
  $axios.defaults.baseURL = app.$env.API_URL
  $axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false })
  $axios.onRequest((cfg) => {
    const token = store.state.auth.token
    cfg.headers.common['Content-Type'] = 'application/json'
    cfg.headers.common.Accept = 'application/json, text/plain, */*'
    if (token) {
      cfg.headers.common.Authorization = `Token ${token}`
    }
  })
  $axios.onError((error) => {
    return new Promise((resolve, reject) => {
      const statusCode = error.response ? error.response.status : -1

      if (statusCode === 401) {
        const textCode = error.response.data.detail || null
        if (textCode === 'Invalid token.' || textCode === 'Token expired.' || textCode === 'Недопустимый токен.') {
          store.dispatch('auth/clearAuth')
          redirect('/')
        }
      }

      return reject(error)
    })
  })
  $axios.defaults.paramsSerializer = params => qs.stringify(params, { arrayFormat: 'repeat', skipNulls: true })
  const axiosRepo = create($axios)

  const repos = {
    v1: axiosRepo('v1'),
  }
  inject('api', repos)
}
