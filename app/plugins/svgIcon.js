import Vue from 'vue'
import SvgIcon from 'vue-svgicon'
import '@/assets/icons'

Vue.use(SvgIcon, { tagName: 'SvgIcon' })
