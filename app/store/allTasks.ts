import { ActionTree, GetterTree, MutationTree } from 'vuex'
import { AllTasksState } from '~/types/storeTypes/allTasks'
import { RootState } from '~/types/store'
import { Task } from '~/types/storeTypes/tasks'

export const state:AllTasksState = {
  tasks: [],
  tags: [],
  pageTasks: 1,
  search: '',
}

export const mutations: MutationTree<AllTasksState> = {
  setTasks (state, payload: Task []) {
    state.tasks = [...state.tasks, ...payload]
  },
  setEmptyTasks (state, payload: Task []) {
    state.tasks = payload
    state.pageTasks = 1
  },
  setTags (state, tags) {
    state.tags = tags.map((item) => {
      return { ...item, active: false, accept: false }
    })
  },
  setSearch (state, text: string) {
    state.search = text
  },
  setTagActive (state, tag) {
    const tagItem = state.tags.find(item => item.id === tag.id)
    if (tagItem !== undefined) {
      tagItem.active = !tagItem.active
    }
  },
  setClearTag (state) {
    state.tags = state.tags.map((item) => {
      if (item.active && !item.accept) {
        item.active = false
      } else if (!item.active && item.accept) {
        item.active = true
      }

      return item
    })
  },
  setAcceptTag (state) {
    state.tags = state.tags.map((item) => {
      if (item.active && !item.accept) {
        item.accept = true
      } else if (!item.active && item.accept) {
        item.accept = false
      }

      return item
    })
  },
  setNext (state, next) {
    if (next !== null) {
      state.pageTasks += 1
    } else {
      state.pageTasks = 0
    }
  },
  clearNext (state) {
    state.tasks = []
    state.pageTasks = 1
  },
  setCancelTags (state) {
    state.tags = state.tags.map((item) => {
      item.accept = false
      item.active = false
      return item
    })
  },
  clear (state) {
    state.tasks = []
  },
}

export const actions: ActionTree<AllTasksState, RootState> = {
  async loadAllTasksPage ({ commit, rootGetters, dispatch }) {
    const query = {
      mine: false,
      page: 1,
    }
    if (rootGetters['auth/isAuthenticated']) {
      const response = await this.app.$api.v1.get('help_application', query)
      if (response?.results) {
        commit('setEmptyTasks', response?.results)
        commit('setNext', response.next)
        dispatch('loader/handleLoader', false, { root: true })
      }
    }
  },
  async loadAllTasks ({ commit, rootGetters, state, getters, dispatch }, next = false) {
    try {
      const query = {
        mine: false,
        tags: '',
        q: state.search,
        page: 1,
      }
      if (state.pageTasks !== 0 && next) {
        query.page = state.pageTasks
      }
      if (getters.counterAcceptTag !== 0) {
        query.tags = state.tags.filter(item => item.accept).map(filterTag => filterTag.name).join(',')
      }
      if (rootGetters['auth/isAuthenticated'] && state.pageTasks !== 0) {
        const response = await this.app.$api.v1.get('help_application', query)
        if (response?.results) {
          commit('setTasks', response?.results)
          if (next) {
            commit('setNext', response.next)
          }
          dispatch('loader/handleLoader', false, { root: true })
        }
      }
    } catch (e) {}
  },
  handleObserverTasks ({ dispatch }) {
    dispatch('loadAllTasks', true)
  },
  async loadTags ({ commit }) {
    try {
      const response = await this.app.$api.v1.get('tags')
      commit('setTags', response?.results)
    } catch (e) {}
  },
  handleTag ({ commit }, tag) {
    commit('setTagActive', tag)
  },
  handleClearTag ({ commit }) {
    commit('setClearTag')
  },
  acceptTags ({ commit, dispatch }) {
    dispatch('loader/handleLoader', true, { root: true })
    commit('setAcceptTag')
    commit('clearNext')
    dispatch('loadAllTasks', true)
  },
  cancelTags ({ commit, dispatch }) {
    dispatch('loader/handleLoader', true, { root: true })
    commit('setCancelTags')
    commit('clearNext')
    dispatch('loadAllTasks', true)
  },
  clearAllTasks ({ commit }) {
    commit('clear')
  },
  handleSearch ({ dispatch, commit }, text: string) {
    commit('setSearch', text)
    commit('clearNext')
    dispatch('loadAllTasks', true)
  },
}

export const getters: GetterTree<AllTasksState, RootState> = {
  counterAcceptTag (state) {
    return state.tags.filter(item => item.accept).length
  },
}
