import { ActionTree, Module, MutationTree, GetterTree } from 'vuex'
// @ts-ignore
import Cookies from 'js-cookie'
import { AuthState, RootState, User } from '~/types/store'
import { LoginForm, ProfileForm } from '~/types/forms'
const cookieparser = process.server ? require('cookieparser') : undefined

export const state:AuthState = {
  token: null,
  isLoading: false,
  user: null,
}

export const mutations: MutationTree<AuthState> = {
  setToken (state, token: string|null) {
    state.token = token
  },
  setUser (state, user: User) {
    state.user = user
  },
}

export const actions: ActionTree<AuthState, RootState> = {
  setTokenFromCookie ({ commit }, { req }) {
    let token = null
    if (req?.headers.cookie) {
      try {
        token = JSON.parse(cookieparser.parse(req.headers.cookie).token).auth.token
      } catch (e) {
        // No valid cookie found
      }
    }
    if (token) {
      commit('setToken', token)
    }
  },
  handleToken ({ commit }, payload: string) {
    Cookies.set('token', payload)
    commit('setToken', payload)
  },
  handleLogout ({ commit }) {
    Cookies.remove('token')
    commit('setToken', null)
    this.$router.replace('/login')
  },
  async handleLogin ({ dispatch }, payload: LoginForm) {
    const data = {
      username: payload.email,
      password: payload.password,
    }

    try {
      const response = await this.app.$api.v1.post('auth', data)
      if (response?.token) {
        dispatch('handleToken', response?.token)
      }
      await this.$router.push('/')
    } catch (e) {
      if (e.response.status === 400) {
        dispatch('snackbar/handlerSnackbar', { active: true, valid: false, message: e.response.data?.non_field_errors[0] }, { root: true })
      }
    }
  },
  async load ({ commit, getters }) {
    if (getters.isAuthenticated) {
      try {
        const response = await this.app.$api.v1.get('user/info')
        commit('setUser', response)
      } catch (e) {}
    }
  },
  async handleProfile ({ dispatch, state }, profileForm: ProfileForm) {
    try {
      const response = await this.app.$api.v1.put(`user/${state.user.id}`, profileForm)
      if (profileForm.avatar !== null) {
        await dispatch('deleteImages')
        const formData = new FormData()
        formData.append('img', profileForm.avatar)
        await this.app.$api.v1.put(`user/${state.user?.id}/image`, formData)
      }
      if (response?.id) {
        await dispatch('load')
      }
    } catch (e) {}
  },
  async deleteImages ({ state }) {
    try {
      for (const img of state.user.images) {
        const id = img?.id
        await this.app.$api.v1.delete(`user/${state.user?.id}/image/${id}`)
      }
    } catch (e) {}
  },
  clearAuth ({ commit }) {
    Cookies.remove('token')
    commit('setToken', null)
  },
}

export const getters: GetterTree<AuthState, RootState> = {
  isAuthenticated (state) {
    return state.token !== null
  },
}

export const auth: Module<AuthState, RootState> = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
