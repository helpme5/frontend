import { ActionTree, MutationTree } from 'vuex'
import { ChatsState } from '~/types/storeTypes/chats'
import { RootState } from '~/types/store'
import { Chat } from '~/types/storeTypes/chat'

export const state: ChatsState = {
  chats: [],
  page: 1,
}

export const mutations: MutationTree<ChatsState> = {
  setEmptyChats (state, payload: Chat []) {
    state.chats = payload
  },
  setChats (state, payload: Chat []) {
    state.chats = [...state.chats, ...payload]
  },
  setNext (state, next) {
    if (next !== null) {
      state.page += 1
    } else {
      state.page = 0
    }
  },
  clearNext (state) {
    state.chats = []
    state.page = 1
  },
}

export const actions: ActionTree<ChatsState, RootState> = {
  async loadChats ({ commit, dispatch }) {
    try {
      const query = {
        me: true,
        page: 1,
      }
      const response = await this.app.$api.v1.get('chat', query)
      if (response?.results) {
        commit('setEmptyChats', response?.results)
        commit('setNext', response.next)
        dispatch('loader/handleLoader', false, { root: true })
      }
    } catch (e) {}
  },
  async loadChatsObserver ({ dispatch, commit, state }) {
    try {
      const query = {
        me: true,
        page: state.page,
      }
      if (state.page !== 0) {
        const response = await this.app.$api.v1.get('chat', query)
        if (response?.results) {
          commit('setChats', response?.results)
          commit('setNext', response.next)
          dispatch('loader/handleLoader', false, { root: true })
        }
      }
    } catch (e) {}
  },
}
