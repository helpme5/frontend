import { ActionTree, MutationTree } from 'vuex'
import { RootState } from '~/types/store'
import { Task, TasksState } from '~/types/storeTypes/tasks'
import { TaskForm } from '~/types/forms'

export const state: TasksState = {
  tasks: [],
  tags: [],
  feedbacks: [],
  page: 1,
}

export const mutations: MutationTree<TasksState> = {
  setTasks (state, payload: Task []) {
    state.tasks = payload
  },
  setTasksObserver (state, payload: Task []) {
    state.tasks = [...state.tasks, ...payload]
  },
  setFeedbacks (state, payload) {
    state.feedbacks = payload
  },
  deleteTask (state, id: number) {
    const taskIndex = state.tasks.findIndex(item => item.id === id)
    if (taskIndex !== -1) {
      state.tasks.splice(taskIndex, 1)
    }
  },
  setNext (state, next) {
    if (next !== null) {
      state.page += 1
    } else {
      state.page = 0
    }
  },
  setTags (state, payload) {
    state.tags = payload.map((item) => {
      return { ...item, active: false }
    })
  },
  clear (state) {
    state.tasks = []
    state.page = 1
  },
  clearPage (state) {
    state.page = 1
  },
  clearFeedbacks (state) {
    state.feedbacks = []
  },
}

export const actions: ActionTree<TasksState, RootState> = {
  async loadTasks ({ commit, dispatch }, load = false) {
    try {
      const response = await this.app.$api.v1.get('help_application', { mine: true })
      commit('setTasks', response?.results)
      if (load) {
        commit('clearPage')
      }
      commit('setNext', response?.next)
      dispatch('loader/handleLoader', false, { root: true })
    } catch (e) {}
  },
  async handleObserverTasks ({ commit, state }) {
    try {
      const page = state.page
      if (page !== 0) {
        const response = await this.app.$api.v1.get('help_application', { mine: true, page })
        if (response?.results.length > 0) {
          commit('setTasksObserver', response?.results)
          commit('setNext', response?.next)
        }
      }
    } catch (e) {}
  },
  async loadTags ({ commit }) {
    try {
      const response = await this.app.$api.v1.get('tags')
      commit('setTags', response?.results)
    } catch (e) {}
  },
  async loadFeedbacks ({ commit, rootState }) {
    try {
      const response = await this.app.$api.v1.get(`help_application/${rootState.modal.context?.id}/offer_help`)
      if (response?.results) {
        commit('setFeedbacks', response?.results)
      }
    } catch (e) {}
  },
  clearTasks ({ commit }) {
    commit('clear')
  },
  async createTask ({ dispatch, rootState, commit }, formTask: TaskForm) {
    try {
      let response = null
      if (!formTask.isChange) {
        response = await this.app.$api.v1.post('help_application', formTask.taskData)
      } else {
        const task = rootState.modal.context
        response = await this.app.$api.v1.put(`help_application/${task?.id}`, formTask.taskData)
      }
      if (response?.id) {
        if (formTask.taskImages.length > 0) {
          for (const image of formTask.taskImages) {
            const formData = new FormData()
            formData.append('img', image.img)
            await this.app.$api.v1.put(`help_application/${response?.id}/image`, formData)
          }
        }
      }
      commit('clearPage')
      dispatch('loadTasks')
      dispatch('modal/closeModal', null, { root: true })
    } catch (e) {}
  },
  async deleteTask ({ commit, dispatch, rootState }) {
    try {
      const contextModal = rootState.modal.context?.id
      await this.app.$api.v1.delete(`help_application/${contextModal}`)
      commit('deleteTask', contextModal)
      dispatch('modal/closeModal', null, { root: true })
    } catch (e) {}
  },
  async handleFeedback ({ dispatch, rootState }, payload) {
    try {
      await this.app.$api.v1.put(`help_application/${rootState.modal.context?.id}/offer_help/${payload.offer}/accept`, { accept: payload.accept })
      await this.app.$api.v1.delete(`help_application/${rootState.modal.context?.id}/offer_help/${payload.offer}`)
      dispatch('modal/closeModal', null, { root: true })
    } catch (e) {}
  },
  handleClearFeedback ({ commit }) {
    commit('clearFeedbacks')
  },
}
