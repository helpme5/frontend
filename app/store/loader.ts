import { ActionTree, MutationTree } from 'vuex'
import { LoaderState } from '~/types/storeTypes/loader'
import { RootState } from '~/types/store'

export const state: LoaderState = {
  active: false,
}

export const mutations: MutationTree<LoaderState> = {
  setLoader (state, status: boolean) {
    state.active = status
  },
}

export const actions: ActionTree<LoaderState, RootState> = {
  handleLoader ({ commit }, status: boolean) {
    commit('setLoader', status)
  },
}
