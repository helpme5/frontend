import { ActionTree, MutationTree } from 'vuex'
import { Chat, ChatState } from '~/types/storeTypes/chat'
import { RootState } from '~/types/store'

export const state:ChatState = {
  messages: [],
  chatInfo: null,
  socket: false,
}

export const mutations: MutationTree<ChatState> = {
  setChat (state, chat: Chat) {
    state.chatInfo = chat
  },
  setEmptyMessages (state, payload) {
    state.messages = payload
  },
  setSocket (state, socket) {
    state.socket = socket
  },
  clearChat (state) {
    state.messages = []
    state.chatInfo = null
  },
}

export const actions: ActionTree<ChatState, RootState> = {
  async loadChat ({ commit }, id: string) {
    try {
      const response = await this.app.$api.v1.get(`chat/${id}`)
      if (response?.id) {
        commit('setChat', response)
      }
    } catch (e) {}
  },
  async loadEmptyMessages ({ commit, state }) {
    try {
      const response = await this.app.$api.v1.get(`chat/${state.chatInfo?.id}/messages/?limit=250&offset=250'`)
      if (response?.results) {
        commit('setEmptyMessages', response?.results)
      }
    } catch (e) {}
  },
  reconnectSocket ({ commit }, status) {
    commit('setSocket', status)
  },
  handleClearChat ({ commit }) {
    commit('clearChat')
  },
}
