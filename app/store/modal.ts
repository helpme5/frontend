import { ActionTree, Module, MutationTree } from 'vuex'
import { ModalState, RootState } from '~/types/store'

export interface ModalOpen {
  component: String|null,
  context: Object|String|null,
}

export const state: ModalState = {
  open: false,
  context: null,
  component: null,
  modalAction: null,
}

export const mutations: MutationTree<ModalState> = {
  open (state, { component }) {
    state.open = true
    state.component = component
  },
  close (state) {
    state.open = false
  },
  setContext (state, context) {
    state.context = context
  },
  destroyComponent (state) {
    state.component = null
  },
  modalAction (state, action) {
    state.modalAction = action
  },
}

export const actions: ActionTree<ModalState, RootState> = {
  openModal ({ commit }, payload:ModalOpen) {
    commit('open', { component: payload.component })
    commit('setContext', payload.context)
  },
  closeModal ({ commit }) {
    commit('close')
    commit('destroyComponent')
    commit('setContext', null)
  },
  setModalAction ({ commit }, payload) {
    commit('modalAction', payload)
  },
}

export const modal: Module<ModalState, RootState> = {
  namespaced: true,
  state,
  mutations,
  actions,
}
