import { ActionTree, Module, MutationTree } from 'vuex'
import { RegistrationAuthForm, RegistrationState, RegistrationStep, RegistrationUserForm, RootState } from '~/types/store'

export const state: RegistrationState = {
  step: 'auth',
  formAuth: {
    email: '',
    password: '',
  },
  avatar: null,
}

export const mutations: MutationTree<RegistrationState> = {
  setStep (state, step: RegistrationStep) {
    state.step = step
  },
  setFormAuth (state, formAuth: RegistrationAuthForm) {
    state.formAuth = formAuth
  },
}

export const actions: ActionTree<RegistrationState, RootState> = {
  registrationStep ({ commit }, step: RegistrationStep) {
    commit('setStep', step)
  },
  formAuth ({ commit }, formAuth: RegistrationAuthForm) {
    commit('setFormAuth', formAuth)
    commit('setStep', 'user')
  },
  async registrationUser ({ state, commit }, formUser: RegistrationUserForm) {
    try {
      const data = {
        ...state.formAuth,
        name: formUser.name,
        surname: formUser.surname,
        description: formUser.description,
      }
      const response = await this.app.$api.v1.post('user', data)
      if (response?.id && formUser.avatar !== '') {
        const formData = new FormData()
        formData.append('img', formUser.avatar)
        await this.app.$api.v1.put(`user/${response?.id}/image`, formData)
      }
      commit('setStep', 'auth')
      await this.$router.push('/login')
    } catch (e) {}
  },
}

export const registration: Module<RegistrationState, RootState> = {
  namespaced: true,
  state,
  actions,
  mutations,
}
