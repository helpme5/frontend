export function state () {
  return {}
}

export const mutations = {}

export const actions = {
  async nuxtServerInit ({ dispatch }, ctx) {
    await dispatch('auth/setTokenFromCookie', ctx)
    await dispatch('auth/load', ctx)
  },
}
