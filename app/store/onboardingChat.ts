import { ActionTree, Module, MutationTree } from 'vuex'
import { OnboardingChat, RootState, Theme } from '~/types/store'

export const state: OnboardingChat = {
  theme: null,
  step: 'about',
  themes: [],
}

export const mutations: MutationTree<OnboardingChat> = {
  setTheme (state, payload: Theme) {
    state.theme = payload
  },
  setStep (state, status: string) {
    state.step = status
  },
}

export const actions: ActionTree<OnboardingChat, RootState> = {
  handleTheme ({ commit }, payload: Theme) {
    commit('setTheme', payload)
  },
  handleStep ({ commit }, status: string) {
    commit('setStep', status)
  },
}

export const onboardingChat: Module<OnboardingChat, RootState> = {
  namespaced: true,
  state,
  mutations,
  actions,
}
