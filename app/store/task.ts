import { ActionTree, MutationTree } from 'vuex'
import { RootState, TaskState } from '~/types/store'

export const state: TaskState = {
  feedback: {
    isHave: false,
  },
  task: null,
}

export const mutations: MutationTree<TaskState> = {
  setFeedback (state, status: boolean) {
    state.feedback.isHave = status
  },
  setTask (state, task) {
    state.task = task
  },
  setLoadFeedback (state, payload) {
    const feedback = payload.feedback.find(item => item.user.id === payload.user.id)
    state.feedback.isHave = feedback !== undefined
  },
}

export const actions: ActionTree<TaskState, RootState> = {
  async sendFeedback ({ commit, state, rootState }, payload) {
    try {
      const user = rootState.auth.user
      const data = {
        message: payload.comment,
        user: {
          email: user.email,
          name: user.name,
          surname: user.surname,
        },
      }
      const response = await this.app.$api.v1.post(`help_application/${state.task.id}/offer_help`, data)
      if (response?.message) {
        commit('setFeedback', true)
      }
    } catch (e) {
      console.error(e)
    }
  },
  async loadFeedback ({ commit, state, rootState }) {
    try {
      const response = await this.app.$api.v1.get(`help_application/${state.task.id}/offer_help`)
      commit('setLoadFeedback', { feedback: response?.results, user: rootState.auth.user })
    } catch (e) {}
  },
  async loadTask ({ commit }, id: string) {
    try {
      const response = await this.app.$api.v1.get(`help_application/${id}`)
      if (response?.id) {
        commit('setTask', response)
      }
    } catch (e) {}
  },
}
