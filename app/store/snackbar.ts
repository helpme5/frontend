import { Module, MutationTree, ActionTree } from 'vuex'
import { RootState, SnackbarState } from '~/types/store'

export const state: SnackbarState = {
  snackbar: {
    active: false,
    valid: true,
    message: '',
  },
}

export const mutations: MutationTree<SnackbarState> = {
  setSnackbar (state, payload) {
    state.snackbar = payload
  },
}

export const actions: ActionTree<SnackbarState, RootState> = {
  handlerSnackbar ({ commit }, payload: SnackbarState) {
    commit('setSnackbar', payload)
  },
}

export const snackbar: Module<SnackbarState, RootState> = {
  namespaced: true,
  state: () => { return { ...state } },
  mutations,
  actions,
}
