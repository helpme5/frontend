module.exports = {
  content: [
    'components/**/*.{vue,js}',
    'containers/**/*.{vue,js}',
    'layouts/**/*.vue',
    'pages/**/*.vue',
    'composables/**/*.{js,ts}',
    'plugins/**/*.{js,ts}',
    'App.{js,ts,vue}',
    'app.{js,ts,vue}',
  ],
  theme: {
    screens: {
      lg: { max: '991px' },
      md: { max: '767px' },
      sm: { max: '479px' },
    },
    extend: {},
  },
  plugins: [],
}
