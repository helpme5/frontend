
type Payload = string|object|null|undefined

interface Config {
  params: Object | null,
}

export default $axios => (resource:string) => ({
  get (path: string, query: Payload) {
    const config: Config = {
      params: null,
    }
    if (query !== null) {
      config.params = query
    }
    return $axios.$get(`/api/${resource}/${path}/`, config)
  },
  post (path: string, payload: Payload) {
    return $axios.$post(`/api/${resource}/${path}/`, payload)
  },
  put (path: string, payload: Payload) {
    return $axios.$put(`/api/${resource}/${path}/`, payload)
  },
  patch (path: string, payload:Payload) {
    return $axios.$patch(`/api/${resource}/${path}/`, payload)
  },
  delete (path: string) {
    return $axios.$delete(`/api/${resource}/${path}/`)
  },
})
