import path from 'path'
const environment = process.env.ENVIRONMENT || 'local'
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 3000,
  },
  head: {
    title: 'app',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/normalize.css',
    '~/assets/css/main.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/api.js',
    '~/plugins/svgIcon.js',
    '~/plugins/lazyImage.js',
    { src: '~/plugins/persistedState.js', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/eslint-module',
    '@nuxtjs/composition-api/module',
    '@nuxtjs/vuetify',
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/sentry',
    [
      'nuxt-env',
      {
        keys: [
          {
            key: 'ENVIRONMENT',
            default: environment,
          },
          {
            key: 'API_URL',
            default: process.env.API_URL,
          },
          {
            key: 'WS_URL',
            default: process.env.WS_URL,
          },
          {
            key: 'IMAGE_URL',
            default: `${process.env.API_URL}/media/`,
          },
          {
            key: 'SENTRY_DSN',
            default: process.env.SENTRY_DSN,
          },
          {
            key: 'NODE_ENV',
            default: process.env.NODE_ENV || 'dev',
          },
        ],
      },
    ],
  ],

  axios: {
    https: true,
    proxy: true,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    cssSourceMap: true,

    extend (config, { isClient }) {
      config.devtool = isClient ? 'eval-source-map' : 'inline-source-map'
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))

      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        use: ['babel-loader', 'vue-svg-loader'],
      })

      config.resolve.alias['~'] = path.resolve(__dirname)
      config.resolve.alias['@'] = path.resolve(__dirname)
    },
    extractCSS: true,
  },

  sentry: {
    dsn: process.env.SENTRY_DSN,
    disabled: environment === 'local' || !process.env.SENTRY_DSN,
    config: {
      environment,
    },
  },
}
