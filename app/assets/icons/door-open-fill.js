/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'door-open-fill': {
    width: 35,
    height: 35,
    viewBox: '0 0 35 35',
    data: '<g clip-path="url(#clip0_74_445)"><path pid="0" d="M3.281 32.812a1.094 1.094 0 000 2.188H31.72a1.094 1.094 0 000-2.188h-3.282V5.47a3.281 3.281 0 00-3.28-3.282h-1.095V1.094A1.094 1.094 0 0022.817.01L7.503 2.198a1.094 1.094 0 00-.94 1.083v29.531H3.28zM24.063 4.375h1.093a1.094 1.094 0 011.094 1.094v27.343h-2.188V4.375zm-5.47 17.5c-.603 0-1.093-.98-1.093-2.188 0-1.207.49-2.187 1.094-2.187.604 0 1.093.98 1.093 2.187 0 1.208-.49 2.188-1.093 2.188z" _fill="#C1C1C1"/></g><defs><clipPath id="clip0_74_445"><path pid="1" _fill="#fff" d="M0 0h35v35H0z"/></clipPath></defs>'
  }
})
