/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'infoSnackbar': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12z" _stroke="#DE0B6E" stroke-width="2.5"/><path pid="1" d="M10.583 7.238a1.42 1.42 0 112.838 0 1.42 1.42 0 01-2.838 0z" _fill="#DE0B6E"/><rect pid="2" x="13.224" y="18.064" width="2.451" height="8.429" rx="1.225" transform="rotate(-180 13.224 18.064)" _fill="#DE0B6E"/>'
  }
})
