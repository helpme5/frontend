/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'house-fill': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M12 4.94l9 9v6.31a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 20.25v-6.31l9-9zm7.5-1.19V9l-3-3V3.75a.75.75 0 01.75-.75h1.5a.75.75 0 01.75.75z" _fill="#C1C1C1"/><path pid="1" fill-rule="evenodd" clip-rule="evenodd" d="M10.94 2.25a1.5 1.5 0 012.12 0l9.971 9.969a.75.75 0 01-1.062 1.062L12 3.311l-9.969 9.97a.751.751 0 01-1.062-1.062l9.97-9.969z" _fill="#C1C1C1"/>'
  }
})
