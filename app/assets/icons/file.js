/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'file': {
    width: 20,
    height: 20,
    viewBox: '0 0 20 20',
    data: '<g clip-path="url(#clip0_60_46)"><path pid="0" d="M17 5.917V17a2.333 2.333 0 01-2.333 2.333H5.333A2.333 2.333 0 013 17V3A2.333 2.333 0 015.333.667h6.417L17 5.917zm-3.5 0a1.75 1.75 0 01-1.75-1.75V1.833H5.333A1.167 1.167 0 004.167 3v14a1.167 1.167 0 001.166 1.167h9.334A1.167 1.167 0 0015.833 17V5.917H13.5z" _fill="#5765CF"/></g><defs><clipPath id="clip0_60_46"><path pid="1" _fill="#fff" transform="translate(.667 .667)" d="M0 0h18.667v18.667H0z"/></clipPath></defs>'
  }
})
