/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'send-fill': {
    width: 16,
    height: 16,
    viewBox: '0 0 16 16',
    data: '<g clip-path="url(#clip0_59_33)"><path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M15.964.686a.501.501 0 00-.65-.65L.767 5.856H.766l-.452.18a.5.5 0 00-.082.886l.41.26v.002l4.996 3.178 3.178 4.995.002.002.26.41a.5.5 0 00.886-.083l6-15zm-1.833 1.89l.47-1.178-1.177.471L5.93 9.363l.338.215a.5.5 0 01.154.154l.215.338 7.494-7.494z" _fill="#fff"/></g><defs><clipPath id="clip0_59_33"><path pid="1" _fill="#fff" d="M0 0h16v16H0z"/></clipPath></defs>'
  }
})
