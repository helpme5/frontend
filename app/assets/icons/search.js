/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'search': {
    width: 19,
    height: 18,
    viewBox: '0 0 19 18',
    data: '<g clip-path="url(#clip0_36_291)"><path pid="0" d="M14.018 11.637a7.263 7.263 0 00-1.15-9.874A7.423 7.423 0 007.782.008a7.412 7.412 0 00-4.936 2.138A7.281 7.281 0 00.69 7.038a7.271 7.271 0 001.776 5.04 7.43 7.43 0 009.966 1.132h-.001c.034.045.07.087.11.13l4.372 4.33a1.14 1.14 0 001.606 0 1.12 1.12 0 000-1.59l-4.37-4.332a1.136 1.136 0 00-.131-.112v.001zm.293-4.325c0 .813-.162 1.618-.476 2.368a6.184 6.184 0 01-1.353 2.008 6.25 6.25 0 01-2.026 1.341 6.296 6.296 0 01-4.78 0 6.25 6.25 0 01-2.026-1.341A6.184 6.184 0 012.297 9.68 6.138 6.138 0 013.65 2.937a6.274 6.274 0 014.416-1.812c1.656 0 3.245.652 4.416 1.812a6.159 6.159 0 011.829 4.375z" _fill="#AAA"/></g><defs><clipPath id="clip0_36_291"><path pid="1" _fill="#fff" transform="translate(.686)" d="M0 0h18.166v18H0z"/></clipPath></defs>'
  }
})
