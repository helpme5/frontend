/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'deleteImage': {
    width: 16,
    height: 16,
    viewBox: '0 0 16 16',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M4.423 3.244a.833.833 0 10-1.179 1.179L6.821 8l-3.577 3.577a.833.833 0 101.179 1.179L8 9.179l3.577 3.577a.833.833 0 001.179-1.179L9.179 8l3.577-3.577a.833.833 0 10-1.179-1.179L8 6.821 4.423 3.244z" _fill="#fff"/>'
  }
})
