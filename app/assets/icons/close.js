/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'close': {
    width: 20,
    height: 20,
    viewBox: '0 0 20 20',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M15.59 4.41a.833.833 0 010 1.18l-10 10a.833.833 0 01-1.18-1.18l10-10a.833.833 0 011.18 0z" _fill="#fff"/><path pid="1" fill-rule="evenodd" clip-rule="evenodd" d="M4.41 4.41a.833.833 0 011.18 0l10 10a.833.833 0 01-1.18 1.18l-10-10a.833.833 0 010-1.18z" _fill="#fff"/>'
  }
})
