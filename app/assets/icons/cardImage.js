/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'cardImage': {
    width: 16,
    height: 16,
    viewBox: '0 0 16 16',
    data: '<g clip-path="url(#clip0_60_65)" _fill="#D0BD8B"><path pid="0" d="M6.002 5.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0z"/><path pid="1" d="M1.5 2A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13zm13 1a.5.5 0 01.5.5v6l-3.775-1.947a.5.5 0 00-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 00-.63.062L1.002 12v.54A.503.503 0 011 12.5v-9a.5.5 0 01.5-.5h13z"/></g><defs><clipPath id="clip0_60_65"><path pid="2" _fill="#fff" d="M0 0h16v16H0z"/></clipPath></defs>'
  }
})
