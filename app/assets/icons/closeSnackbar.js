/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'closeSnackbar': {
    width: 20,
    height: 20,
    viewBox: '0 0 20 20',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M16.995 4.775a1.25 1.25 0 00-1.768-1.768l-5.226 5.227-5.226-5.226a1.25 1.25 0 10-1.767 1.768l5.225 5.225-5.226 5.227a1.25 1.25 0 001.768 1.767L10 11.77l5.227 5.228a1.25 1.25 0 001.768-1.768L11.768 10l5.227-5.226z" fill="#1E1E1E" fill-opacity=".2"/>'
  }
})
