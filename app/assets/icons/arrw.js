/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'arrw': {
    width: 9,
    height: 14,
    viewBox: '0 0 9 14',
    data: '<path pid="0" d="M1.231.574A1.48 1.48 0 013.31.672l4.303 4.693c.85.927.85 2.343 0 3.27L3.31 13.328a1.48 1.48 0 01-2.078.098 1.45 1.45 0 01-.098-2.06l3.88-4.232a.2.2 0 000-.268l-3.88-4.232A1.45 1.45 0 011.23.574z" _fill="#C1C1C1"/>'
  }
})
