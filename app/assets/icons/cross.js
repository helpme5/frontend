/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'cross': {
    width: 10,
    height: 10,
    viewBox: '0 0 10 10',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M8.36 2.525a.625.625 0 00-.884-.884L5 4.117 2.526 1.642a.625.625 0 00-.884.884L4.117 5 1.642 7.475a.625.625 0 10.883.884l2.476-2.475L7.476 8.36a.625.625 0 10.884-.884L5.884 5 8.36 2.525z" _fill="#fff"/>'
  }
})
