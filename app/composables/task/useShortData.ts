import { computed } from '@nuxtjs/composition-api'

export default function useShortData (props) {
  return {
    shortTitle: computed(() => {
      if (props?.title.length > 25) {
        return `${props?.title.substring(0, 25)}...`
      }
      return props?.title
    }),
    shortDescription: computed(() => {
      if (props?.description.length > 55) {
        return `${props?.description.substring(0, 55)}...`
      }
      return props?.description
    }),
    shortMessage: computed(() => {
      if (props?.message === null) {
        return 'Введите сообщение...'
      }
      if (props?.message.length > 35) {
        return `${props?.message.substring(0, 35)}...`
      }
      return props?.message
    }),
    shortMessageCount: computed(() => {
      if (props?.count > 99) {
        return '99+'
      }
      return props?.count
    }),
  }
}
