import { computed } from '@nuxtjs/composition-api'

export default function dateCreate (props) {
  return {
    dateCreate: computed(() => {
      const parseDate = new Date(Date.parse(props?.task.created_at))
      return parseDate.toLocaleString('ru', {
        day: 'numeric',
        month: 'short',
        year: 'numeric',
      })
    }),
  }
}
