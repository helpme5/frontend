import { useContext } from '@nuxtjs/composition-api'

export default function useIsAuth () {
  const { store, redirect } = useContext()
  const isAuth: boolean = store.getters['auth/isAuthenticated']
  if (isAuth) {
    redirect('/')
  }
}
