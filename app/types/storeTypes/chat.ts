import { TaskImage } from '~/types/storeTypes/tasks'

export interface ChatUser {
  id: number,
  email: string,
  name: string,
  surname: string,
  images: TaskImage [],
}

export interface Chat {
  id: number,
  room_code: string,
  application: {
    id: number,
    title: string,
  },
  helper: ChatUser,
  taker: ChatUser,
}

export interface ChatState {
  messages: [],
  chatInfo: Chat,
  socket: any,
}
