import { AllTag, Task } from '~/types/storeTypes/tasks'

export interface AllTasksState {
  tasks: Task [],
  tags: AllTag [],
  pageTasks: number,
  search: string,
}
