export interface TaskImage {
  id: number,
  img: string,
}
export interface Tag {
  id: number,
  name: string,
  active: boolean,
}

export interface AllTag {
  id: number,
  name: string,
  active: boolean,
  accept: boolean,
}
export interface Task {
  id: number,
  tags: string [],
  title: string,
  description: string,
  is_anonymous: boolean,
  images: TaskImage [],
  place: string,
}

export interface TasksState {
  tasks: Task [],
  tags: Tag[],
  feedbacks: [],
  page: number,
}
