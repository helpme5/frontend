import { Chat } from '~/types/storeTypes/chat'

export interface ChatsState {
  chats: Chat [],
  page: number,
}
