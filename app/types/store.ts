import { Task, TaskImage, TasksState } from '~/types/storeTypes/tasks'
import { AllTasksState } from '~/types/storeTypes/allTasks'
import { LoaderState } from '~/types/storeTypes/loader'
import { ChatsState } from '~/types/storeTypes/chats'
import { ChatState } from '~/types/storeTypes/chat'

export interface User {
  email: string,
  name: string,
  id: number,
  surname: string,
  description: string,
  images: TaskImage [],
}

export interface AuthState {
  token: string|null,
  user: User|null,
  isLoading: boolean,
}

export interface SnackbarState {
  snackbar: {
    active: boolean,
    valid: boolean,
    message: string,
  },
}

export interface ModalState {
  open: boolean,
  context: Task | null,
  component: String | null,
  modalAction: String | null,
}

export interface TaskState {
  feedback: {
    isHave: boolean,
  },
  task: Task,
}

export interface Theme {
  id: number,
  name: string,
}

export interface OnboardingChat {
  step: string,
  themes: (Theme | string) [],
  theme: Theme | null,
}

export type RegistrationStep = 'auth' | 'user'

export interface RegistrationForm {
  email: string,
  name: string,
  surname: string,
  description: string,
  password: string,
}

export interface RegistrationAuthForm {
  email: string,
  password: string,
}

export interface RegistrationUserForm {
  name: string,
  surname: string,
  description: string,
  avatar: any,
  avatarUrl: string,
}

export interface RegistrationState {
  step: RegistrationStep,
  formAuth: RegistrationAuthForm,
  avatar: string | null,
}

export interface RootState {
  auth: AuthState,
  snackbar: SnackbarState,
  modal: ModalState,
  task: TaskState,
  onboardingChat: OnboardingChat,
  registration: RegistrationState,
  tasksProfile: TasksState,
  allTasks: AllTasksState,
  loader: LoaderState,
  chats: ChatsState,
  chat: ChatState,
}
