import { TaskImage } from '~/types/storeTypes/tasks'

export interface LoginForm {
  email: string,
  password: string,
}

export interface ProfileForm {
  name: string,
  surname: string,
  description: string,
  email: string,
  avatar: string,
}

export interface TaskDataForm {
  title: string,
  description: string,
  is_anonymous: boolean,
  place: string,
  tags: string [],
}

export type TaskImagesForm = TaskImage []

export interface TaskForm {
  taskData: TaskDataForm,
  taskImages: TaskImagesForm,
  isChange: boolean,
}

export interface RootForms {
  login: LoginForm,
  profile: ProfileForm,
  task: TaskForm,
}
